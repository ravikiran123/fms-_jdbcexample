package com.centerpoint.fms.daoImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.centerpoint.fms.entity.UserDetails;
import com.centerpoint.fms.iDao.IUserDetailsDao;

@Repository
public class UserDetailsDaoImpl implements IUserDetailsDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public UserDetails saveUserDetails(UserDetails userDetails) {
		try {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			jdbcTemplate.update("INSERT INTO user_details(user_id,user_name,user_address)VALUES(?,?,?)",
					userDetails.getUser_id(), userDetails.getUser_name(), userDetails.getUser_address());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<UserDetails> getusers() {
		String sql = "SELECT * FROM user_details";
		List UserDetailsList = new ArrayList();
		jdbcTemplate.query(sql, new ResultSetExtractor() {
			public List extractData(ResultSet rs) throws SQLException {
				while (rs.next()) {
					UserDetails u = new UserDetails();
					Integer user_id = rs.getInt("user_id");
					u.setUser_id(user_id);
					String user_name = rs.getString("user_name");
					u.setUser_name(user_name);
					String user_address = rs.getString("user_address");
					u.setUser_address(user_address);
					UserDetailsList.add(u);
				}
				return UserDetailsList;
			}
		});
		return UserDetailsList;
	}

	@Override
	public UserDetails getuser(Integer user_id) {
		String sql = "SELECT * FROM user_details where user_id="+user_id;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sql);
		return null;
	}
}
