package com.centerpoint.fms.iService;

import java.util.List;

import com.centerpoint.fms.entity.UserDetails;

public interface IUserDetailsService {

	public UserDetails saveUserDetails(UserDetails userDetails);

	public List<UserDetails> getusers();

	public UserDetails getuser(Integer user_id);
	
}
