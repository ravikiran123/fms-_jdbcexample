package com.centerpoint.fms.controller;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.centerpoint.fms.entity.UserDetails;
import com.centerpoint.fms.iService.IUserDetailsService;

@RestController
public class UserDetailsController {
	
	@Autowired
	private IUserDetailsService iService;

	
	@PostMapping(value="/saveUserDetails")
	public UserDetails saveUserDetails(@RequestBody UserDetails userDetails) {
		UserDetails details = iService.saveUserDetails(userDetails);
		return null;
	}
	
	@GetMapping("/getUsers")
	public List<UserDetails> getUserDetailss(){
		List<UserDetails> list = iService.getusers();
		return list;
	}
	
	@GetMapping("/{user_id}")
	public UserDetails getuser(@PathVariable Integer user_id) {
		UserDetails user = iService.getuser(user_id);
		return user;
	}
	
	
	
}
