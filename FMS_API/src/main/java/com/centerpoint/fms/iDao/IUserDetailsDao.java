package com.centerpoint.fms.iDao;

import java.util.List;

import com.centerpoint.fms.entity.UserDetails;

public interface IUserDetailsDao {

	public UserDetails saveUserDetails(UserDetails userDetails);

	public List<UserDetails> getusers();

	public UserDetails getuser(Integer user_id);
	
}
